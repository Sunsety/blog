﻿using Blog.Data.Infrastructure;
using Blog.Data.Repositories;
using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Service
{
    public interface ICommentService
    {
        IEnumerable<Comment> GetGadgets();
      //  IEnumerable<Comment> GetCategoryGadgets(string categoryName, string gadgetName = null);
        Comment GetGadget(int id);
        void CreateGadget(Comment gadget);
        void SaveGadget();
    }

    public class GadgetService : ICommentService
    {
        private readonly ICommentRepository gadgetsRepository;
        private readonly IArticleRepository categoryRepository;
        private readonly IUnitOfWork unitOfWork;

        public GadgetService(ICommentRepository gadgetsRepository, IArticleRepository categoryRepository, IUnitOfWork unitOfWork)
        {
            this.gadgetsRepository = gadgetsRepository;
            this.categoryRepository = categoryRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IGadgetService Members

        public IEnumerable<Comment> GetGadgets()
        {
            var gadgets = gadgetsRepository.GetAll();
            return gadgets;
        }

        //public IEnumerable<Comment> GetCategoryGadgets(string categoryName, string gadgetName = null)
        //{
        //    var category = categoryRepository.GetCategoryByName(categoryName);
        //    return category..Where(g => g.Name.ToLower().Contains(gadgetName.ToLower().Trim()));
        //}

        public Comment GetGadget(int id)
        {
            var gadget = gadgetsRepository.GetById(id);
            return gadget;
        }

        public void CreateGadget(Comment gadget)
        {
            gadgetsRepository.Add(gadget);
        }

        public void SaveGadget()
        {
            unitOfWork.Commit();
        }

        #endregion

    }
}
