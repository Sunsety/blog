﻿using Blog.Data.Infrastructure;
using Blog.Data.Repositories;
using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Service
{
    // operations you want to expose
    public interface IArticleService
    {
       // IEnumerable<Article> GetGadgets();
       // IEnumerable<Article> GetCategoryGadgets(string categoryName, string gadgetName = null);
       // Article GetGadget(int id);
       // void CreateGadget(Comment gadget);
       // void SaveGadget();
    }

    public class ArticleService : IArticleService
    {
        private readonly ICommentRepository gadgetsRepository;
        private readonly IArticleRepository categoryRepository;
        private readonly IUnitOfWork unitOfWork;

        public ArticleService(ICommentRepository gadgetsRepository, IArticleRepository categoryRepository, IUnitOfWork unitOfWork)
        {
            this.gadgetsRepository = gadgetsRepository;
            this.categoryRepository = categoryRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IGadgetService Members

        public IEnumerable<Comment> GetGadgets()
        {
            var gadgets = gadgetsRepository.GetAll();
            return gadgets;
        }

        //public IEnumerable<Article> GetCategoryGadgets(string categoryName, string gadgetName = null)
        //{
        //    var category = categoryRepository.GetCategoryByName(categoryName);
        //    return category.Gadgets.Where(g => g.Name.ToLower().Contains(gadgetName.ToLower().Trim()));
        //}

        //public Article GetGadget(int id)
        //{
        //    var gadget = gadgetsRepository.GetById(id);
        //    return gadget;
        //}

        //public void CreateGadget(Article gadget)
        //{
        //    gadgetsRepository.Add(gadget);
        //}

        public void SaveGadget()
        {
            unitOfWork.Commit();
        }

        #endregion

    }
}

