﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Model.Models
{
   public class Article
    {
        public int Id { get; set; }
        public string Tit { get; set; } //заголовок статьи
        public string Text { get; set; }
        public DateTime DateCreated { get; set; }

        public Article()
        {
            DateCreated = DateTime.Now;
        }

    }
}
