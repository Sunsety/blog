﻿using Blog.Data;
using Blog.Model.Models;
using Blog.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IArticleService categoryService;
        private readonly ICommentService gadgetService;

        BlogEntities context = new BlogEntities("BlogEntities");

        // GET: Home
        public ActionResult Index()
        {
           
            return View(context.Articles);
        }
        [HttpGet]
        public ActionResult AddComent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddComent(Comment comment)
        {
            context.Comments.Add(comment);
            context.SaveChanges();
            return RedirectToAction("Comments");
        }
        [HttpGet]
        public ActionResult Poll()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Poll(Poll poll)
        {
            context.Polls.Add(poll);
            context.SaveChanges();
            return View("ResultPoll",poll);
         
        }
        public ActionResult ListPolls()
        {
            return View(context.Polls);
        }
        [HttpGet]
        public ActionResult Comments()
        {

            return View(context.Comments);
        }
        public ActionResult ResultPoll()
        {
            return View();
        }
    }

}
