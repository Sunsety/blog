﻿using Blog.Data.Infrastructure;
using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data.Repositories
{
    public class ArticleRepository : RepositoryBase<Article>, IArticleRepository
    {
        public ArticleRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public Article GetCategoryByName(string categoryName)
        {
            var category = this.DbContext.Articles.Where(c => c.Tit == categoryName).FirstOrDefault();

            return category;
        }

        public override void Update(Article entity)
        {
           // entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
    }

    public interface IArticleRepository : IRepository<Article>
    {
        Article GetCategoryByName(string categoryName);
    }
}
