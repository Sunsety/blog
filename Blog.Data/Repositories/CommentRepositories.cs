﻿using Blog.Data.Infrastructure;
using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }

    public interface ICommentRepository : IRepository<Comment>
    {

    }
}
