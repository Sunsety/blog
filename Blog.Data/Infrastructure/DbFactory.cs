﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        BlogEntities dbContext;

        public BlogEntities Init()
        {
            return dbContext ?? (dbContext = new BlogEntities("BlogEntities"));
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
