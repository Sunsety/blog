﻿using Blog.Data.Configuration;
using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data
{
    public class BlogEntities : DbContext
    {
        public BlogEntities(string BlogEntities) : base(BlogEntities) { }

        static BlogEntities()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<BlogEntities>());
            Database.SetInitializer(new BlogSeedData());

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Poll> Polls { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ArticleConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
        }
    }
}
