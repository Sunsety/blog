﻿using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data
{
   public class BlogSeedData : DropCreateDatabaseIfModelChanges<BlogEntities>
    {
        protected override void Seed(BlogEntities context)
        {
            GetArticles().ForEach(c => context.Articles.Add(c));
            GetComments().ForEach(g => context.Comments.Add(g));

            context.Commit();
        }
        private static List<Article> GetArticles()
        {
            return new List<Article>
            {
                new Article {
                    Tit = "Article1",
                    Text = "blabalbal",
                    
                },
                new Article {
                    Tit = "Article2",
                    Text = "hahahahaaha"
                },
            };
        }

        private static List<Comment> GetComments()
        {
            return new List<Comment>
            {
                new Comment {
                    Text = "Good app",
                    byAuthor = "User1",
                    
                   
                },
            };
        }
    }
}
