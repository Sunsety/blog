﻿using Blog.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data.Configuration
{
    class CommentConfiguration : EntityTypeConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            Property(c => c.Text).IsRequired().HasMaxLength(100);
            Property(c => c.byAuthor).IsRequired().HasMaxLength(100);
        }
    }
}
